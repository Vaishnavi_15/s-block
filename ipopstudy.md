# FACE RECOGNITION

| **Input/Output info** | **AMAZON REKOGNITION** | **MICROSOFT COGNITIVE SERVICES** |
| ------ | ------ | ------ |
|**Features**|Facial Recognition, Facial analysis, Face comparison, Image moderation etc| Face verification, Emotion detection, Face recognition, Face detection etc.|
| **Input : Pythonapi to feed input** |`IndexFaces` : The parameters are FaceModelVersion, MaxFaces, QualityFilter, detectionAttributes.  | C# :`DetectWithUrlAsync()` or `DetectWithStreamAsync()`: The parameters are operations, url, returnFaceID, returnFaceLandmarks, returnFaceAttributes, recognitionModel, detectionModel and cancellation.|
|**Input data for images** | Amazon Rekognition Image supports the ***JPEG and PNG*** image formats(image file sizes up to *15MB when passed as an S3 object*, and up to *5MB when submitted as an image byte array*). |The input image formats should be ***JPEG, PNG, GIF*** (the first frame) with *image size not more than 4 MB*. The detectable face size range is 36 x 36 to 4096 x 4096 pixels.|
|**Input data for videos** | The supported video file formats are *MPEG-4 and MOV* and the stored video must be encoded using the *H. 264*. Amazon Rekognition Video supports up to 10 GB files and up to 6 hour videos when passed through as an S3 file. |For detecting faces from a video feed, turn off the smoothing effect of video camera, shutter speed should be preferably 1/60 sec. or faster and lower the shutter angle.|
|**Essentials**| *boto3* AWS Software Development Kit (SDK) | *face Client library* for python |
|**Output method**|***1. Bounding box*** : Amazon face Rekognition returns bounding boxes coordinates for face/items that are detected in images.|**1.** Get the ***locations and dimensions of faces*** in an image.|
|          |***2. Confidence*** :Confidence level that the bounding box contains a face (and not a different object such as a tree).|**2.** Get the ***locations of various face landmarks***, such as pupils, nose, and mouth, in an image.|
|          |***3. ExternalImageId*** :Identifier that you assign to all the faces in the input image. |**3.** Get the gender, age, emotion, and other ***attributes of a detected face***.|
|          | ***4.Emotion*** :The emotions that appear to be expressed on the face, and the confidence level in the determination.|            |
|          |***5. Image quality*** :Identifies face image brightness and sharpness. |         |
|          |***6. Label*** : Structure containing details about the detected label, including the name, detected instances, parent labels, and level of confidence. |       |
|          |***7. Gender*** : Amazon Rekognition makes gender binary (male/female) predictions based on the physical appearance of a face in a particular image.|   |
|**Output format** | JSON | JSON|

#### MY OPINION
I will prefer Amazon Rekognition over MS Cognitive Services as Amazon Rekognition detects faces more accurately and provide more labels and in Rekognition Python language is used, but in MS Cognitive Services C# language is used.

 
