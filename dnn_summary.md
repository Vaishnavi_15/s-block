# WHAT IS A NEURAL NETWORK?
Neural Network is used to store information in computer, it consists of nodes. It uses connection weights of neurons to store information. In the artificial intelligence field, artificial neural networks have been applied successfully to speech recognition, image analysis and adaptive control, in order to construct software agents or autonomous robots.

<img src="/uploads/158f15a8738e2cfe0c99df75d418f647/Farqui.png" width="900" height="550">

### Y = g(v), where g = Activation function

## TYPES OF NEURAL NETWORK
1. **Single Layer Neural Network:** It only has input and output layer.
2. **Shallow / Vanilla Neural Network:** It has one hidden layer between input and output layer.
3. **Deep Neural Network:** It has two or more than two hidden layers between input and output layers.

### Neural network examples
<img src="https://cdn.analyticsvidhya.com/wp-content/uploads/2018/10/Screenshot-from-2018-10-12-14-10-51.png">

<img src="/uploads/e4c265d2a58f2413c54fd75819152e48/Untitled_Diagram__3_.jpg" width="800">

<img src="https://miro.medium.com/max/834/0*B_2wAzU14ush5uBO.">
<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTQ7OlmNXgXDucdYxiK1xtDZR1CNNRl1PMT_w&usqp=CAU" width="500">

#### Where alpha= learning rate, W= weight of layer L

<img src="https://image.slidesharecdn.com/artificialneuralnetwork-170311194336/95/artificial-neural-network-20-638.jpg?cb=1489261441" width="900" height="650">

## HOW NEURAL NETWORKS LEARN?
1. Randomly initialize weights in the neural network.
2. Send the first set of input values to the neural network and propagate values trough it to get the output value.
3. Compare output value to the expected output value and calculate the error using cost functions.
4. Propagate the error back to the network and set the weights according to that information.
5. Repeat steps from 2 to 4 for every input value we have in our training set.
6. When the entire training set has been sent through the neural network, one epoch has been finished. After that, we repeat more epochs.

### Learning rate
- The learning rate determines how fast the optimal weights for the model are calculated.
- A smaller learning rate may lead to more accurate weights (up to a certain point), but the time it takes to compute the weights will be longer.

### Activation functions
It determines the behaviour of node. There are various activation functions used in classification. Some of them are Sigmoid function, tanh function, ReLU function and Leaky ReLU function. Sigmoid function is preferred only in Binary classification. In most of the cases, ReLU(Rectified Linear Unit) function is preffered. Some of the Activation functions have been shown below:
<img src="https://i.pinimg.com/originals/44/3d/25/443d25218a81e4bd8bac8d954c866a1b.png" width="900" height="450">

## BACKPROPAGATION
- Backpropagation, short for "backward propagation of errors," is an algorithm for supervised learning of artificial neural networks using gradient descent. 
- Given an artificial neural network and an error function, the method calculates the gradient of the error function with respect to the neural network's weights.
- It is a generalization of the delta rule for perceptrons to multilayer feedforward neural networks.
>#### Code 
<img src="https://miro.medium.com/max/952/1*ohfvtAN6AOV8mhBwr4PApQ.png">

#### where, Z[L] = W[L] (A[L-1]) + b[L], A[L] = g[L] (Z[L]), g=Activation function, Y=[y(1), y(2),…………….y(m)]
